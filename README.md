# README #

### Mobile Partner Optimizer for Windows ###

* Optimizes Mobile Partner for less resource usage
* Compatible Version [23.015.11.01.983](http://download-c1.huawei.com/download/downloadCenter?downloadId=55222&version=175478&siteCode=)

### How do I get set up? ###

* Run `install.bat` with administrator privilages

### Contribution guidelines ###

* Use `developer` branch

### Who do I talk to? ###

* Repo [admin](https://bitbucket.org/ashenmgunaratne/)

